//
//  SoGoSurvye.swift
//  MobileFramework
//
//  Created by Rohit Mane on 12/3/20.
//  Copyright © 2020 Rohit Mane. All rights reserved.
//

import Foundation
import UIKit

public class SoGoSurvey : UIViewController {
    
    public static let shared = SoGoSurvey()
    
//    private init(){
//        self.modalPresentationStyle = .fullScreen
//    }
    
    override init(nibName: String?, bundle: Bundle?) {
        super.init(nibName: nibName, bundle: bundle)
        self.modalPresentationStyle = .fullScreen
    }
    
    public override func viewDidLoad() {
         
     }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func showCustomAlert(view : UIViewController,message :String,submitBtnName:String){
//        let vc = CustomAlertViewController()
//        vc.message = message
//        vc.submitBtnText = submitBtnName
//        view.present(vc, animated: true)
    }
    
    public func showAlertWithTextView(view : UIViewController,message :String,submitBtnName:String){
//            let vc = TextPopUpViewController()
//            vc.message = message
//            vc.submitBtnName = submitBtnName
//            view.present(vc, animated: true)
    }
    
    public func showAlertWithYesNoTypeView(view : UIViewController,message :String,yesLabelName:String,noLabelName:String,submitButtonName:String){
//            let vc = YesNoPopupViewController()
//            vc.message =  message
//            vc.yesLableName = yesLabelName
//            vc.noLableName = noLabelName
//            vc.submitButtonName = submitButtonName
//            view.present(vc, animated: true)
    }
    
    public func showAlert(title: String, message: String,  view:UIViewController,options: [String]) {
        presentAlertWithTitle(title: title, message: message, view: view , options: options) { (option) in
                    print("option: \(option)")
                    switch(option) {
                        case "start":
                            print("start button pressed")
                            break
                        case "stop":
                            print("stop button pressed")
                            break
                        case "cancel":
                            print("cancel button pressed")
                            break
                        default:
                            break
                    }
        }
    }
   
    
    public func presentAlertWithTitle(title: String, message: String, view:UIViewController,options: [String], completion: @escaping (String) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(options[index])
            }))
        }
        view.present(alertController, animated: true, completion: nil)
    }
    
    public func enterScreen(screenName: String,view: UIViewController){
        
        let seconds = 4.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            let vc = SurveyWebview()
            view.present(vc, animated: true)
        }
        
//        let s = UIStoryboard (
//            name: "MainStoryboard", bundle: Bundle(for: self)
//        )
//        let vc = s.instantiateViewController(withIdentifier: "SurveyWebview") as! UIViewController
//        self.navigationController?.pushViewController(vc, animated: true)
//        self.presentViewController(vc, animated: false, completion: nil)
        
    }
    
    public func invokeEvent(screenName : String,view: UIViewController){
      
        let vc = SurveyWebview()
        view.present(vc, animated: true)
    }
}
